#ifndef RESPONSE_H
#define RESPONSE_H

typedef struct position_t{
    double latitude;
    double longitude;
}position_t;

typedef struct weather_t{
    double temp;
    double feels_like;
    double temp_min;
    double temp_max;
    double pressure;
    double humidity;
}weather_t;

position_t getPosition(char * response_data);
weather_t getWeather(char * response_data);
#endif