/* Includes */
#include <stdio.h>
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "freertos/semphr.h"
#include "led.h"
#include "wifi.h"
#include "http_client.h"
#include <pthread.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "response.h"
#include <string.h>

/* DEFINES */
#define LED_DELAY 1000       // 1s
#define REQUEST_DELAY 300000 // 5min

/* Semaphores */
xSemaphoreHandle connectedWifiSemaphore;
xSemaphoreHandle httpHandlerSemaphore;
xSemaphoreHandle ledHandlerSemaphore;

/* Constants */
#define IP_STACK_KEY CONFIG_ESP_IP_STACK_KEY
#define OW_KEY CONFIG_ESP_OW_KEY

/* Global variables*/
bool connected = false;
char *response_buffer;
int response_size;
char url_buffer[2050];

/* Handler funcitons header */
void connectionHandler(void *params);
void ledHandler(void *params);
void httpHandler(void *params);
void blink_led();
char *get_ip_stack_url();
char *get_ow_url(position_t pos);

/* Main */
void app_main(void)
{

  led_init();

  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  connectedWifiSemaphore = xSemaphoreCreateBinary();
  ledHandlerSemaphore = xSemaphoreCreateBinary();
  httpHandlerSemaphore = xSemaphoreCreateBinary();

  xTaskCreate(&ledHandler, "Cuida do led", 1024, NULL, 1, NULL);
  xTaskCreate(&connectionHandler, "Cuida das conexoes", 2048, NULL, 1, NULL);
  xTaskCreate(&httpHandler, "Cuida das requisições", 4096, NULL, 1, NULL);

  xSemaphoreGive(ledHandlerSemaphore);
  wifi_start();
}

void connectionHandler(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(connectedWifiSemaphore, portMAX_DELAY))
    {
      ESP_LOGI("Main Task", "WIFI Connected");
      xSemaphoreGive(httpHandlerSemaphore);
    }
  }
}

void ledHandler(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(ledHandlerSemaphore, portMAX_DELAY))
    {
      if (connected)
      {
        turn_led_on();
        vTaskDelay(LED_DELAY / portTICK_PERIOD_MS);
      }
      else
      {
        blink_led();
      }
      xSemaphoreGive(ledHandlerSemaphore);
    }
  }
}

void httpHandler(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(httpHandlerSemaphore, portMAX_DELAY))
    {
      ESP_LOGI("Main Task", "Making HTTP Request");

      /* Request to stack url API*/
      http_request(get_ip_stack_url());
      position_t pos = getPosition(response_buffer);
      free(response_buffer);
      response_size = 0;

      /* Request to open weather API */
      http_request(get_ow_url(pos));
      weather_t wt = getWeather(response_buffer);
      free(response_buffer);
      response_size = 0;
      printf("Temperatura atual: %.2lf ºC\n", wt.temp);
      printf("Temperatura máxima prevista: %.2lf ºC\n", wt.temp_max);
      printf("Temperatura mínima prevista: %.2lf ºC\n", wt.temp_min);
      printf("Humidade: %.2lf %%\n", wt.humidity);

      blink_led();
      vTaskDelay(REQUEST_DELAY / portTICK_PERIOD_MS);
      xSemaphoreGive(httpHandlerSemaphore);
    }
  }
}

void blink_led()
{
  turn_led_off();
  vTaskDelay(LED_DELAY / portTICK_PERIOD_MS);
  turn_led_on();
  vTaskDelay(LED_DELAY / portTICK_PERIOD_MS);
}

char *get_ip_stack_url()
{
  sprintf(url_buffer, "%s%s",
          "http://api.ipstack.com/45.4.43.136?access_key=", IP_STACK_KEY);
  return url_buffer;
}
char *get_ow_url(position_t pos)
{
  sprintf(url_buffer, "%s%lf%s%lf%s%s",
          "http://api.openweathermap.org/data/2.5/weather?lat=", pos.latitude,
          "&lon=", pos.longitude, "&appid=", OW_KEY);
  return url_buffer;
}