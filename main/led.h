#ifndef LED_H
#define LED_H
#define LED_PIN 2

void led_init();
void turn_led_on();
void turn_led_off();

#endif
