#include "response.h"
#include "cJSON.h"
#include <stdio.h>

position_t getPosition(char *response_data)
{
    cJSON *json = cJSON_Parse(response_data);
    position_t pos;
    pos.latitude = cJSON_GetObjectItemCaseSensitive(json, "latitude")->valuedouble;
    pos.longitude = cJSON_GetObjectItemCaseSensitive(json, "longitude")->valuedouble;
    cJSON_Delete(json);
    return pos;
}

weather_t getWeather(char *response_data)
{
    cJSON *json = cJSON_Parse(response_data);
    cJSON *weather = cJSON_GetObjectItemCaseSensitive(json, "main");
    weather_t wt;
    double kelvin_factor = -273.15;
    wt.temp = kelvin_factor + cJSON_GetObjectItemCaseSensitive(weather, "temp")->valuedouble;
    wt.feels_like = kelvin_factor + cJSON_GetObjectItemCaseSensitive(weather, "feels_like")->valuedouble;
    wt.temp_min = kelvin_factor + cJSON_GetObjectItemCaseSensitive(weather, "temp_min")->valuedouble;
    wt.temp_max = kelvin_factor + cJSON_GetObjectItemCaseSensitive(weather, "temp_max")->valuedouble;
    wt.pressure = cJSON_GetObjectItemCaseSensitive(weather, "pressure")->valuedouble;
    wt.humidity = cJSON_GetObjectItemCaseSensitive(weather, "humidity")->valuedouble;
    cJSON_Delete(json);
    return wt;
}