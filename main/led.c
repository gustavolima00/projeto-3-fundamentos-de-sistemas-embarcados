#include "led.h"
#include "driver/gpio.h"

void led_init(){
    gpio_reset_pin(LED_PIN);
    gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);
}
void turn_led_on(){
    gpio_set_level(LED_PIN, 1);
}

void turn_led_off(){
    gpio_set_level(LED_PIN, 0);
}